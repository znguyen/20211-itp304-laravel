<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">

	<title>Genres | Song DB</title>
</head>
<body>

	<div class="container">
		<div class="row">
			<h1 class="col-12 mt-4">Genres</h1>
		</div>
	</div> <!-- .container -->

	<div class="container">
		<div class="row">
			<ul class="list-group my-3 col-12 col-md-6">

				<li class="list-group-item">
					Genre Name
				</li>

			</ul>
		</div> <!-- .row -->
	</div> <!-- .container -->
	
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>